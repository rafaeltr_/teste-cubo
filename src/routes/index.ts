import express from "express";
import RulesController from "../controllers/rules.controller";

const router = express.Router();

router.post("/registerRules", (req, res) => {
  try{
    const controller = new RulesController();
    const response = controller.registerRulesController(req.body);
    return res.status(200).send(response)
  }
  catch(err){
    res.status(400).json(err)
  }
});

router.get("/listRules", (req, res) => {
  const controller = new RulesController();
  try{
    const response = controller.listRulesController();
    return res.status(200).send(response)
  }
  catch(err){
    res.status(400).json(err)
  }
});

router.get("/listFreeTimes", (req, res) => {
  const controller = new RulesController();
  try{
    const initialDate = req.query.initialDate
    const finalDate =  req.query.finalDate
    const response = controller.listFreeTimesController(initialDate, finalDate);
    return res.status(200).send(response)
  }
  catch(err){
    res.status(400).json(err)
  }
});

router.delete("/deleteRule", (req, res) => {
  const controller = new RulesController();
  try{
    const response = controller.deleteRuleController(req.body);
    return res.status(200).send(response)
  }
  catch(err){
    res.status(400).json(err)
  }
});


export default router;
