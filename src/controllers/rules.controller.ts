import { Post, Get, Delete,Route, Body, Query, Example } from "tsoa";
import { FreeTimes, IntervalRuleRegister , Register} from '../models/rules.model'
import { registerRule } from '../repositories/rules.repository'
import { getRules } from '../repositories/rules.repository'
import { deleteRule } from '../repositories/rules.repository'
import { listFreeTimes } from '../repositories/rules.repository'
@Route("")
export default class RegisterSchedule {

  @Post("/registerRules")
  public registerRulesController(@Body() body: IntervalRuleRegister ): Promise<IntervalRuleRegister> {
   return registerRule(body)
  }

  @Get("/listRules")
  public listRulesController(): IntervalRuleRegister {
    return getRules()
  }

  @Get("/listFreeTimes")
  public listFreeTimesController(@Query() initialDate: any, @Query() finalDate: any ): FreeTimes {
    return listFreeTimes(initialDate, finalDate)
  }

  @Delete("/deleteRule")
  @Example("Regra deletada")
  public deleteRuleController(@Body() body: Register): string {
    return deleteRule(body)
  }
}

