import fs from 'fs';
import {IntervalRuleRegister} from '../models/rules.model'

function listRule(): IntervalRuleRegister | null {
    const data = fs.readFileSync("db/data.json", "utf8")
    if (data) {
        return JSON.parse(data)
    }
    return null
}

export default listRule