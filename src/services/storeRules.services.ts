import fs from 'fs';
import { IntervalRuleRegister, Register } from '../models/rules.model';
import listRule from './listRules.services'
import { throwError, verifyIfDateIsValid, getDayOfWeek } from '../utils/index';



function findByDay(registerOne: Register, registerTwo: Register): boolean {
  if (registerOne.day && registerTwo.weekly) {
      const nameOfDay = getDayOfWeek(registerOne.day)
      const includes = registerTwo.weekly.days.includes(nameOfDay.toLowerCase())
      if (includes)
          return registerOne.start >= registerTwo.start && registerOne.start <= registerTwo.end
      return false
  }
  return registerOne.start >= registerTwo.start && registerOne.start <= registerTwo.end && registerOne.day === registerTwo.day
}

function findByEveryDay(registerOne: Register, registerTwo: Register): boolean {
  //verificar se os horarios estão em conflitos
  if (registerOne.start >= registerTwo.start && registerOne.start <= registerTwo.end)
      return true
  if (registerOne.end >= registerTwo.start && registerOne.end <= registerTwo.end)
      return true
  return false

}


function validatePayloads(newRegister: Register, registerIntoDB: Register): boolean {
  if (registerIntoDB.everyDay || newRegister.everyDay) {
      return findByEveryDay(newRegister, registerIntoDB)
  }
  if (registerIntoDB.day && newRegister.weekly) {
      return findByDay(registerIntoDB, newRegister)
  }
  if (registerIntoDB.weekly && newRegister.day) {
      return findByDay(newRegister, registerIntoDB)
  }
  return findByDay(newRegister, registerIntoDB)
}

function validateConflicts(newRegister: Register, registerDB: IntervalRuleRegister, removeFirst?: boolean): Array<Register> {
  const conflicts = registerDB.intervals.filter((registerIntoDB: Register) => {
      if (removeFirst) {
          //removendo o primeiro indice igual do array para nao validar a si mesmo
          const equals = newRegister === registerIntoDB ? true : false
          if (equals) {
              removeFirst = false
              return
          }
      }
      //verificando se existe conflito entre os intervalos de horas e dias
      return validatePayloads(newRegister, registerIntoDB)
  })

  return conflicts
}

export function verifyIfDateHasConflict(newRule: IntervalRuleRegister, registersIntoDB: IntervalRuleRegister | null): void {
  const conflictsRules = newRule.intervals.reduce((acc: Array<Register>, interval: Register) => {

      let hasConflictBetweenDB = registersIntoDB ? validateConflicts(interval, registersIntoDB) : []
      const hasConflictBetweenNewRule: Array<Register> = validateConflicts(interval, newRule, true)
      if (hasConflictBetweenDB.length > 0 || hasConflictBetweenNewRule.length > 0) {
          acc.push(interval)
      }
      return [...acc, ...hasConflictBetweenDB, ...hasConflictBetweenNewRule]
  }, [])

  if (conflictsRules.length > 0) {
      throwError('há um conflito entre as regras, verifique o payload e o banco de dados e tente novamente. Payloads com conflito', conflictsRules)
  }
}


function verifyPayload(newRule: IntervalRuleRegister): void {
  newRule.intervals.every((register: Register) => {
    const hasDay = register.day ? true : false
    const hasWeekly = register.weekly ? true : false
    const hasEveryDay = register.everyDay ? true : false

    if (hasDay && hasWeekly || hasEveryDay && hasWeekly || hasDay && hasEveryDay || hasWeekly && hasEveryDay) {
      throwError('payload não pode conter mais de um parametro de tempo no mesmo registro', [register])
    }
    if (!hasDay && !hasWeekly && !hasEveryDay) {
      throwError('payload deve conter day, weekly ou everyday', [register])
    }
  })
}

function validateRegisterRule(newRule: IntervalRuleRegister): void {
  verifyPayload(newRule);
  const registersIntoDB: IntervalRuleRegister | null = listRule()
  verifyIfDateHasConflict(newRule, registersIntoDB)
  verifyIfDateIsValid(newRule) 
}

function lowerCaseWeekDays(payload: IntervalRuleRegister): IntervalRuleRegister {
  payload.intervals = payload.intervals.map(register => {
    if(register.weekly){
      //lowercase weekdays
     register.weekly.days.map((day: string) => day.toLowerCase())  
    }
    return register
  });

  return payload
}

function insertRule(payload: IntervalRuleRegister): IntervalRuleRegister {
  const oldRule: any = listRule()
  const payloadLower = lowerCaseWeekDays(payload)
    
  //insert into json
  const newRule: IntervalRuleRegister = {
    intervals: payloadLower.intervals
  }
  if (oldRule.intervals.length > 0) {
    newRule.intervals = [...oldRule.intervals, ...newRule.intervals]
  }

  fs.writeFileSync("db/data.json", JSON.stringify(newRule))

  return newRule
}


export {
  insertRule,
  validateRegisterRule
}