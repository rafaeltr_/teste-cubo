import fs from 'fs';
import { Register} from '../models/rules.model'

function deleteRule(body: Register): string {
    const data = fs.readFileSync("db/data.json", "utf8")
    if (data) {
        const Rules = JSON.parse(data)
        const index = Rules.intervals.findIndex((rule:Register) => JSON.stringify(rule) == JSON.stringify(body))
        if (index) {
            Rules.intervals.splice(index, 1)
            fs.writeFileSync("db/data.json", JSON.stringify(Rules), "utf8")
            return "Regra deletada"
        }
    }
    return 'Regra não encontrada'
}

export default deleteRule