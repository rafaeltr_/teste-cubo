import { FreeTimes, Register, Time } from 'src/models/rules.model';
import { formatDate, getDayOfWeek } from '../utils/index';

import listRule from './listRules.services'

function findByEveryDay(days: Array<Register>, date: Date): Array<Time> {
  let time: Array<Time> = []
  days.forEach(day => {
    time.push({ start: day.start, end: day.end })
  })
  return time
}

function findByDay(days: Array<Register>, date: Date): Array<Time> {
  let time: Array<Time> = []
  days.forEach(day => {
    if (day.day && day.day == formatDateFreeTimes(date)) {
      time.push({ start: day.start, end: day.end })
    }
  })
  return time
}

function findByWeekly(days: Array<Register>, date: Date): Array<Time> {
  let time: Array<Time> = []
  days.forEach(day => {
    if (day.weekly && day.weekly.days.includes(getDayOfWeek(date))) {
      time.push({ start: day.start, end: day.end })
    }
  })
  return time
}

function formatDateFreeTimes(date: Date): string {
  return `${date.getDate()}-${date.getMonth() + 1}-${date.getFullYear()}`
}

function verifyIfDateHasConflict(date: Date) {
  const rules = listRule()
  if (!rules)
    return

  const everyDay = [...rules.intervals.filter(rule => rule.everyDay)]
  const days = [...rules.intervals.filter(rule => rule.day)]
  const weekly = [...rules.intervals.filter(rule => rule.weekly)]

  const freeTimes: FreeTimes = { day: formatDateFreeTimes(date), intervals: [] }
  if (everyDay.length > 0)
    freeTimes.intervals.push(...findByEveryDay(everyDay, date))
  if (days.length > 0)
    freeTimes.intervals.push(...findByDay(days, date))
  if (weekly.length > 0)
    freeTimes.intervals.push(...findByWeekly(weekly, date))

  return freeTimes
}

function validateDays(date: Date) {
  return verifyIfDateHasConflict(date)
}

function returnFreeTimes(initialDate: string, finalDate: string) {
  const start: Date = formatDate(initialDate)
  const end: Date = formatDate(finalDate)
  const freeTimes: Array<FreeTimes> = []
  const diff = end.getTime() - start.getTime()
  const diffDays = Math.ceil(diff / (1000 * 3600 * 24))
  for (let i = 0; i <= diffDays; i++) {
    //validar se a data esta de acordo com as regras
    const hasTime = validateDays(new Date(start.getTime() + (i * (1000 * 3600 * 24))))
    if (hasTime)
      freeTimes.push(hasTime)
  }
  return freeTimes

}


export default returnFreeTimes