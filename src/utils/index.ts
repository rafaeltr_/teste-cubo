import { IntervalRuleRegister, Register } from '../models/rules.model';


export function formatDate(date: string): Date {
    const [day, month, year] = date.split('-')
    return new Date(Number(year), Number(month) - 1, Number(day))
}

function stringifyArray(array: Array<Register>): string {
    return array.map((register: Register) => {
        if (register.day) {
            return `Dia: ${register.day}, inicio: ${register.start}, fim: ${register.end}`
        }
        if (register.everyDay) {
            return `Todos os dias, inicio: ${register.start}, fim: ${register.end}`
        }
        if (register.weekly) {
            return `Dias da semana: ${register.weekly.days}, inicio: ${register.start}, fim: ${register.end}`
        }

    }).join('; ')
}


export function throwError(message: string, data?: Array<Register>): void {
    const error = new Error()
    if (data && data.length > 0)
        error.message = `${message} ${stringifyArray(data)}`
    else
        error.message = `${message}`
    throw (error)
}

export function verifyIfDateIsValid(newRule: IntervalRuleRegister, date?: string): void {
    const format = /^\d{2}-\d{2}-\d{4}$/
    if (date) {
        if (!format.test(date))
            throwError(`Data inválida: ${date}`)
    }

    newRule.intervals.filter((rule: Register) => {
        if (rule.day) {

            const isValid = format.test(rule.day)
            if (!isValid)
                throwError('Data inválida', [rule])

            return formatDate(rule.day) < new Date()
        }
        return
    })
}


export function getDayOfWeek(day: string | Date): string {
    let dayOfWeek: string | number
    if (typeof day == 'string')
        dayOfWeek = new Date(formatDate(day)).getDay()
    else
        dayOfWeek = day.getDay()

    const daysOfWeek = ["sunday", "monday", "tuesday", "wednesday", "thursday", "friday", "saturday"]
    return daysOfWeek[dayOfWeek]
}





