import { validateRegisterRule, insertRule } from '../services/storeRules.services'
import listRuleService from '../services/listRules.services';
import deleteRuleService from '../services/deleteRules.services';
import listFreeTimesService from '../services/listFreeTimes.services'
import { IntervalRuleRegister, Register } from '../models/rules.model'


export async function registerRule(body: IntervalRuleRegister): Promise<IntervalRuleRegister> {
    await validateRegisterRule(body);
    const resultRegisterRule: IntervalRuleRegister = await insertRule(body)
    return resultRegisterRule
}

export function getRules(): any {
    return listRuleService();
}

export function listFreeTimes(initialDate: string, finalDate: string): any {
    return listFreeTimesService(initialDate, finalDate);
}

export function deleteRule(body: Register): string {
    return deleteRuleService(body)
}