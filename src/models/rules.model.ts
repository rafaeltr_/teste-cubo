import { Example } from "tsoa";

export class Weekly {
    @Example(["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"])
    public days!: Array<string>;
}

export class Register {
    @Example(true)
    public everyDay?: boolean;

    public weekly?: Weekly;

    @Example("23-10-2020")
    public day?: string;

    @Example("14:20")
    public start!: string;

    @Example("16:50")
    public end!: string;

    @Example(true)
    public is_active?: boolean

    public source?: string;
}

export class IntervalRuleRegister {
    intervals!: Array<Register>
}

export class Time {
    @Example("14:20")
    public start!: string;

    @Example("16:50")
    public end!: string;
}

export class FreeTimes {
    day!: string;
    intervals!: Array<Time>;
}



